@echo off
netsh advfirewall firewall add rule name="allow ktpservice jdk-21 java" profile=domain,private,public protocol=any enable=yes DIR=In program="%ProgramFiles%\Java\jdk-21\bin\java.exe" Action=Allow
netsh advfirewall firewall add rule name="allow ktpservice jdk-21 javaw" profile=domain,private,public protocol=any enable=yes DIR=In program="%ProgramFiles%\Java\jdk-21\bin\javaw.exe" Action=Allow
