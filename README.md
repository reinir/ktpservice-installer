# Penggunaan

Syarat: JDK 21 sudah terinstall dengan konfigurasi default.

Setelah instalasi, ktpservice akan berjalan secara otomatis.

# Untuk development dan debugging

Menjalankan ktpservice pada console (command prompt) berguna untuk development dan debugging aplikasi-aplikasi yang akan berkomunikasi dengan ktpservice.

Jika hal ini diperlukan, lakukan langkah-langkah berikut:
- buka console (command prompt)
- pindah ke directory instalasi ktpservice
- lalu jalankan: `java -jar ktpservice.jar`

Setelah selesai, restart komputer agar ktpservice kembali berjalan secara otomatis.
