@echo off

cd /d "%~dp0"

if "%~1"=="" (
    start "" javaw -jar ktpservice.jar
) else (
    java -jar ktpservice.jar "%~1"
)
