!define /date MYTIMESTAMP "%Y%m%d-%H%M"
Name "ktpservice"
RequestExecutionLevel admin
Outfile "\projects\java\ktpservice-installer-${MYTIMESTAMP}.exe"
InstallDir $PROFILE\ktpservice
Var RetryCount

Section
SetOutPath $INSTDIR
IfFileExists $INSTDIR\ktpservice.jar 0 Start
    DetailPrint "Closing active service..."
    SetDetailsPrint none
    ExecWait "javaw -jar $\"$INSTDIR\ktpservice.jar$\" close"
    SetDetailsPrint both
    StrCpy $RetryCount 0
Start:
    File "ktpservice.jar"
    IfErrors 0 Done
    IntOp $RetryCount $RetryCount + 1
    IntCmp $RetryCount 80 Done
    Sleep 100
    Goto Start
Done:
ExecWait "netsh advfirewall firewall add rule name=$\"allow ktpservice jdk-21 java.exe$\" profile=domain,private,public protocol=any enable=yes DIR=In program=$\"C:\Program Files\Java\jdk-21\bin\java.exe$\" Action=Allow"
ExecWait "netsh advfirewall firewall add rule name=$\"allow ktpservice jdk-21 javaw.exe$\" profile=domain,private,public protocol=any enable=yes DIR=In program=$\"C:\Program Files\Java\jdk-21\bin\javaw.exe$\" Action=Allow"
File "ktpservice.cmd"
File "allow.cmd"
; File "ktpservice.ico"
; SetOutPath $INSTDIR\lib
File "gson-2.10.1.jar"
File "tomcat-annotations-api-11.0.0-M20.jar"
File "tomcat-embed-core-11.0.0-M20.jar"
Exec "$\"$INSTDIR\ktpservice.cmd$\""
WriteUninstaller $INSTDIR\uninstaller.exe
Delete $INSTDIR\ktpservice.exe
Delete $INSTDIR\*.fmd
Delete $INSTDIR\*.log

WriteRegStr   HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "ktpservice" "$\"$INSTDIR\ktpservice.cmd$\""
WriteRegStr   HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\ktpservice" "DisplayName" "ktpservice"
; WriteRegStr   HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\ktpservice" "DisplayIcon" "$\"$INSTDIR\ktpservice.ico$\""
WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\ktpservice" "EstimatedSize" 4040
WriteRegStr   HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\ktpservice" "UninstallString" "$\"$INSTDIR\uninstaller.exe$\""
SectionEnd

Section "Uninstall"
DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Run"
IfFileExists $INSTDIR\ktpservice.jar 0 Done
    DetailPrint "Closing active service..."
    SetDetailsPrint none
    ExecWait "javaw -jar $\"$INSTDIR\ktpservice.jar$\" close"
    SetDetailsPrint both
    StrCpy $RetryCount 0
Start:
    Delete $INSTDIR\ktpservice.jar
    IfErrors 0 Done
    IntOp $RetryCount $RetryCount + 1
    IntCmp $RetryCount 80 Done
    Sleep 100
    Goto Start
Done:
Delete $INSTDIR\ktpservice.cmd
Delete $INSTDIR\ktpservice.exe
Delete $INSTDIR\ktpservice.ico
Delete $INSTDIR\allow.cmd
Delete $INSTDIR\*.log
Delete $INSTDIR\*.jar
Delete $INSTDIR\lib\*.jar
Delete $INSTDIR\log\*.log
Delete $INSTDIR\uninstaller.exe

RMDir $INSTDIR\tomcat.8080\work\Tomcat\localhost\ROOT
RMDir $INSTDIR\tomcat.8080\work\Tomcat\localhost
RMDir $INSTDIR\tomcat.8080\work\Tomcat
RMDir $INSTDIR\tomcat.8080\work
RMDir $INSTDIR\tomcat.8080
RMDir $INSTDIR\tmp\work\Tomcat\localhost\ROOT
RMDir $INSTDIR\tmp\work\Tomcat\localhost
RMDir $INSTDIR\tmp\work\Tomcat
RMDir $INSTDIR\tmp\work
RMDir $INSTDIR\tmp
RMDir $INSTDIR\log
RMDir $INSTDIR\lib
RMDir $INSTDIR

DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\ktpservice"
SectionEnd
